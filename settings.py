import requests
import shutil
import redis
import json
import time
import os
from multiprocessing.dummy import Pool
from subprocess import Popen
from pprint import pprint as pp


BLOCK_CHECK_DEPTH = 10000
MAX_WAIT_TIME_SECONDS = 10

USE_TESTNET = False

if USE_TESTNET:
    HOST = "localhost"
    PORT = "8332"
    LOGIN = "v3ZRyqW45nDHYeTkD8b4"
    PASSWORD = "BkbPS8zf2ZMzXDeMVJ9c"
else:
    HOST = "localhost"
    PORT = "18332"
    LOGIN = "v3ZRyqW45nDHYeTkD8b4"
    PASSWORD = "BkbPS8zf2ZMzXDeMVJ9c"

DIR = os.path.dirname(os.path.realpath(__file__)) + "/"
TEST_ADDRESSES_FILE = DIR + "just_addresses.txt"

# Redis
REDIS_BLOCKS = {
    'host': 'localhost',
    'port': '6379',
    'db': 3,
    'password': None
}
REDIS_UNSPENT_INPUTS = {
    'host': 'localhost',
    'port': '6379',
    'db': 4,
    'password': None
}

