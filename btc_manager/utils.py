from settings import *


def r_post(url, data):
    for i in range(10):
        try:
            r = requests.post(url, data=data)
            return r
        except Exception as exc:
            print("Error connecting to bitcoind:", exc)


def get_redis_connection(credentials):
    redis_conn = redis.StrictRedis(**credentials)
    return redis_conn


def tx_in_redis(redis_conn, tx):
    if redis_conn.get(tx):
        return True
    else:
        return False


def get_our_addresses():
    addresses = set()
    with open(TEST_ADDRESSES_FILE, "r") as f:
        for line in f:
            addresses.add(line.strip())
    return addresses


def check_tx_has_our_address(tx, our_addresses, best_block_height):
    vouts = tx['vout']
    addresses_data = list()
    for vout in vouts:
        if vout['value'] > 0 \
                and 'addresses' in vout['scriptPubKey'] \
                and len(vout['scriptPubKey']['addresses']) == 1:
            address = vout['scriptPubKey']['addresses'][0]
            if address in our_addresses:
                address_data = {
                    'address': address,
                    'value': vout['value'],
                    'tx_id': tx['txid'],
                    'confirmations': best_block_height - tx['height'] + 1
                }
                addresses_data.append(address_data)
    return addresses_data


if __name__ == '__main__':
    pass
