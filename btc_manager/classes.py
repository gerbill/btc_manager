from settings import *
from btc_manager.utils import *


class BTCRPC(object):

    def __init__(self, host=HOST, port=PORT, login=LOGIN, password=PASSWORD):
        self.url = "http://{}:{}@{}:{}".format(login, password, host, port)
        self.redis_conn_blocks = get_redis_connection(REDIS_BLOCKS)
        self.redis_conn_inputs = get_redis_connection(REDIS_UNSPENT_INPUTS)
        self.txs = list()
        self.raw_txs = list()
        self.start_time = time.time()

    def check_deposits(self, our_addresses):
        blocks_txs = self._get_last_blocks_txs()
        best_block_height = self._custom_method("getblockchaininfo")['blocks']
        checked_tx_data = list()
        for block_ids, txs in blocks_txs.items():
            our_txs_in_block = list()
            for tx in txs:
                tx_data = check_tx_has_our_address(tx, our_addresses, best_block_height)
                if tx_data:
                    our_txs_in_block.append(tx_data)
            if our_txs_in_block:
                checked_tx_data += our_txs_in_block
            elif not self.redis_conn_blocks.get("block:{}".format(block_ids[0])):
                self.redis_conn_blocks.set("block:{}".format(block_ids[0]), block_ids[1])
        return checked_tx_data

    def _get_last_blocks_txs(self):
        blocks = [self._get_best_block_hash()]
        all_txs = dict()
        for block in blocks:
            block_from_redis = self.redis_conn_blocks.get('block:{}'.format(block))
            if not block_from_redis:
                txs, prev_block = self._get_block_data(block)
                all_txs[(block, prev_block)] = txs
            else:
                txs, prev_block = [], block_from_redis.decode()
            if (time.time() - self.start_time) > MAX_WAIT_TIME_SECONDS or len(blocks) >= BLOCK_CHECK_DEPTH:
                break
            else:
                blocks.append(prev_block)
        return all_txs

    def _get_block_data(self, block):
        data = self._custom_method('getblock', [block, 2])
        height = data['height']
        prev_block = data['previousblockhash']
        txs = data['tx']
        for tx in txs:
            tx['height'] = height
        return txs, prev_block

    def _get_only_prev_block(self, block):
        data = self._custom_method('getblock', [block])
        prev_block = data['previousblockhash']
        txs = []
        return txs, prev_block

    def _get_best_block_hash(self):
        return self._custom_method('getbestblockhash')

    def _custom_method(self, method, params=None):
        if not params:
            params = []
        return self._send_message_to_bitcoind(method, params)

    def _send_message_to_bitcoind(self, method, params):
        if not isinstance(params, list):
            params = [params]
        data = {
            "jsonrpc": "1.0",
            "method": method,
            "params": params,
            "id": 1
        }
        response = r_post(self.url, json.dumps(data))
        if response:
            response = json.loads(response.text)
            if response["result"]:
                result = response["result"]
                return result
            else:
                print(">> RPC ERROR <<", response["error"])
        else:
            print(response.text)
            print("Bitcoind down")
